#! /usr/bin/env python
from flask import Flask

from .views import app
from . import models

# connect sqlalchemy to app
models.db.init_app(app)


# initialisation de la DB disponible en cli
@app.cli.command('add_data')
def init_db():
    models.Mock.init_db()
