from flask_sqlalchemy import SQLAlchemy
import logging as lg
from enum import Enum
from .views import app

# Create database connection object
db = SQLAlchemy(app)


# Formatage genre
class Gender(Enum):
    female = 0
    male = 1
    other = 0


class Content(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(200), nullable=False)
    gender = db.Column(db.Enum(Gender), nullable=False)

    def __init__(self, description, gender):
        self.description = description
        self.gender = gender


db.create_all()


class Mock:

    @staticmethod
    def init_db():
        try:
            db.drop_all()
            db.create_all()
            db.session.add(
                Content("Thanks to give me the floor, I am Yvon, i'm Java developer! I am 4 years experiences "
                        "in "
                        "Java and 3 years in working in Scrum environment.", Gender['male']))
            db.session.add(
                Content("I am participated in more projects during this experiences, but, 2 projects retain my "
                        "attention to talk about.", Gender['female']))
            db.session.commit()
            lg.info('Database initialized')
        except Exception as e:
            lg.error(e)
