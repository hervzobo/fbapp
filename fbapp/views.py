from flask import Flask, render_template

app = Flask(__name__)

# config options - make sure you created a 'config.py'
app.config.from_object('config')


# To get one variable, tape app.config['MY_VAIRABLE']

@app.route('/')
@app.route('/index/')
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run()
