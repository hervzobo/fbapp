import os
from enum import Enum, auto
from flask import Flask

from flask_session import Session

app = Flask(__name__)
app.config.from_object("config")
app.config["API_TITLE"] = "fbapp"
app.config["APP_VERSION"] = "v0.0.1"

Session(app)


class Env(Enum):
    PRODUCTION = auto()
    TESTING = auto()
    DEVELOPMENT = auto()
